<cfif event.isAdminUser()>
	<cfscript>
		prc.hasCmsPageEditPermissions = prc.hasCmsPageEditPermissions ?: hasCmsPermission( permissionKey="sitetree.edit", context="page", contextKeys=event.getPagePermissionContext() );

		if ( prc.hasCmsPageEditPermissions ) {
			event.include( "/js/admin/presidecore/" );
			event.include( "/js/admin/frontend/" );
			event.includeData({
				  ajaxEndpoint = event.buildAdminLink( linkTo="ajaxProxy.index" )
				, adminBaseUrl = event.getAdminPath()
			});
		}

		event.include( "i18n-resource-bundle" );
		event.include( "/css/admin/core/" );
		event.include( "/css/admin/frontend/" );
		
		if ( hasCmsPermission( "devtools.console" ) ) {
			event.include( "/js/admin/devtools/" )
			     .include( "/css/admin/devtools/" )
			     .includeData( { devConsoleToggleKeyCode=getSetting( "devConsoleToggleKeyCode" ) } );
		}

		userMenu          = renderView( "/admin/layout/userMenu" );
		// TODO: Integrate Notifications
		// notificationsMenu = renderViewlet( "admin.notifications.notificationNavPromo" );


		ckEditorJs = renderView( "admin/layout/ckeditorjs" );

		toggleLiveContentLink = event.buildAdminLink( linkTo="general.toggleNonLiveContent" );
		editPageLink          = event.getEditPageLink();

		event.include( "css-adminRadialMenu" );
		event.include( "js-adminRadialMenu" );
	</cfscript>

	<cfoutput>

		<div id="circularMenu1" class="circular-menu circular-menu-left presidecms">
			<a class="floating-btn" onclick="document.getElementById('circularMenu1').classList.toggle('active');">
			</a>
		
			<menu class="items-wrapper">
				<a href="#event.buildAdminLink()#" class="menu-item">
					<i class="fa fa-home"></i>
				</a>

				<cfif prc.hasCmsPageEditPermissions>
					<a class="menu-item edit-mode-toggle-container">
						<i class="fa fa-edit" id="edit-mode-options"></i>
					</a>

					<a href="#editPageLink#" class="menu-item">
						<i class="fa fa-pencil fa-lg fa-fw"></i>
					</a>

					<a id="showContentBtn" class="menu-item" role="button"">
						<i class="fa fa-eye-slash fa-lg fa-fw"></i>
					</a>

				</cfif>
			</menu>
		</div>
		
		<div id="showContentOptions" style="display:none;">
			<ul class="popupOptions">
				<li>
					<a href="#toggleLiveContentLink#">
						<cfif event.showNonLiveContent()>
							<i class="fa fa-fw smaller-80"></i>
						<cfelse>
							<i class="fa fa-check fa-fw grey smaller-80"></i>
						</cfif>
						#translateResource( 'cms:admintoolbar.show.live.only' )#
					</a>
				</li>
				<li>
					<a href="#toggleLiveContentLink#">
						<cfif event.showNonLiveContent()>
							<i class="fa fa-check fa-fw grey smaller-80"></i>
						<cfelse>
							<i class="fa fa-fw smaller-80"></i>
						</cfif>
						#translateResource( 'cms:admintoolbar.show.non.live' )#
					</a>
				</li>
			</ul>
		</div>
		
		<div id="circularMenu" class="circular-menu circular-menu presidecms">
			<a class="floating-btn" onclick="document.getElementById('circularMenu').classList.toggle('active');">
				<img class="nav-user-photo user-photo" src="//www.gravatar.com/avatar/#LCase( Hash( LCase( event.getAdminUserDetails().email_address ) ) )#?r=g&d=mm&s=50" alt="" />
			</a>
		
			<menu class="items-wrapper">
				<a class="menu-item" href="#event.buildAdminLink( linkTo="login.logout" )#">
					<i class="fa fa-sign-out"></i>
				</a>
				
				<a class="menu-item" href="#event.buildAdminLink( linkTo="editProfile" )#">
					<i class="fa fa-user"></i>
				</a>

			</menu>
		</div>		
		
		#ckEditorJs#
	</cfoutput>
</cfif>


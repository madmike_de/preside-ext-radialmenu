preside-ext-radialmenu
=

Sometimes the systems admin menubar is hiding some details in your website header. If so you need to do some css tricks to move the content down some pixels to make them visivble. 
This is a replacement for the system admin menu. Instead of showing a complete menu bar it shows just two small round icon on the bottom left and right of your page. These icons have (not yet completely, sorry) the same menufunctions as the original menubar.

This is the first version and is missing the following functions:

- insert puffin logo
- the switch for showing live/preview doesn't work yet
- the notfication information is not shown yet (thinking about a good idea)

**How to use the extension:**
It's simple: Add the extension to your `/extensions` folder. 
In your layout-file add `adminRadialMenu = renderView("/general/_adminRadialMenu");` at the beginning of the page (must be before `#event.renderIncludes()#`-call).
Then output your variable `adminRadialMenu` at the end of your page inside the body-tag.

If you have a helping hand or a brilliant idea for this extension, give me a ping.

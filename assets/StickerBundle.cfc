component output=false {

	public void function configure( bundle ) output=false {
		bundle.addAsset( id="css-adminRadialMenu"			, path="/css/adminRadialMenu.css" );
		bundle.addAsset( id="js-adminRadialMenu"			, path="/js/adminRadialMenu.js" );

	}
}
$(function () {
/* TODO: Show on correct position */
	$('#showContentBtn').popover({
		html: true,
		placement: 'top',
		title: '',
		toggle: 'popover',
		content:  $('#showContentOptions').html()
	});	

	$('body').on('click', function (e) {
		if ($(e.target).parents('.circular-menu').length === 0) {
			$('#showContentBtn').popover('hide');
		}
	});

})